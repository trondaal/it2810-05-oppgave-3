"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var platform_browser_1 = require('@angular/platform-browser');
var forms_1 = require('@angular/forms');
var router_1 = require('@angular/router');
var app_component_1 = require('./components/app.component');
var header_component_1 = require('./components/header.component');
var navbar_component_1 = require('./components/navbar.component');
var footer_component_1 = require('./components/footer.component');
var home_component_1 = require('./components/home.component');
var admin_component_1 = require('./components/admin.component');
var privacy_component_1 = require('./components/privacy.component');
var about_component_1 = require('./components/about.component');
var editor_component_1 = require('./components/editor.component');
var login_component_1 = require('./components/login.component');
var signup_component_1 = require('./components/signup.component');
var subjects_component_1 = require('./components/subjects.component');
var texts_component_1 = require('./components/texts.component');
var AppModule = (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        core_1.NgModule({
            imports: [
                platform_browser_1.BrowserModule,
                forms_1.FormsModule,
                router_1.RouterModule.forRoot([
                    { path: 'admin', component: admin_component_1.AdminComponent },
                    { path: 'privacy', component: privacy_component_1.PrivacyComponent },
                    { path: 'about', component: about_component_1.AboutComponent },
                    { path: 'editor', component: editor_component_1.EditorComponent },
                    { path: 'login', component: login_component_1.LoginComponent },
                    { path: 'signup', component: signup_component_1.SignupComponent },
                    { path: 'subjects', component: subjects_component_1.SubjectsComponent },
                    { path: 'texts', component: texts_component_1.TextsComponent },
                    { path: '', component: home_component_1.HomeComponent },
                    { path: '**', component: home_component_1.HomeComponent }
                ])
            ],
            declarations: [app_component_1.AppComponent, footer_component_1.FooterComponent, admin_component_1.AdminComponent, privacy_component_1.PrivacyComponent, header_component_1.HeaderComponent, navbar_component_1.NavbarComponent, home_component_1.HomeComponent, about_component_1.AboutComponent, editor_component_1.EditorComponent, login_component_1.LoginComponent, signup_component_1.SignupComponent, subjects_component_1.SubjectsComponent, texts_component_1.TextsComponent],
            bootstrap: [app_component_1.AppComponent]
        }), 
        __metadata('design:paramtypes', [])
    ], AppModule);
    return AppModule;
}());
exports.AppModule = AppModule;
//# sourceMappingURL=app.module.js.map