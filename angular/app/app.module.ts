import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule }    from '@angular/forms';
import { RouterModule }   from '@angular/router';

import { AppComponent }   from './components/app.component';

import { HeaderComponent }   from './components/header.component';
import { NavbarComponent }   from './components/navbar.component';
import { FooterComponent }   from './components/footer.component';

import { HomeComponent }   from './components/home.component';
import {AdminComponent} from './components/admin.component';
import {PrivacyComponent} from './components/privacy.component';

import {AboutComponent} from './components/about.component';
import {EditorComponent} from './components/editor.component';
import {LoginComponent} from './components/login.component';
import {SignupComponent} from './components/signup.component';
import {SubjectsComponent} from './components/subjects.component';
import {TextsComponent} from './components/texts.component';



@NgModule({
   imports: [
    BrowserModule,
    FormsModule,
    RouterModule.forRoot([
      { path: 'admin', component: AdminComponent },
      { path: 'privacy', component: PrivacyComponent },
      { path: 'about', component: AboutComponent },
      { path: 'editor', component: EditorComponent },
      { path: 'login', component: LoginComponent },
      { path: 'signup', component: SignupComponent },
      { path: 'subjects', component: SubjectsComponent },
      { path: 'texts', component: TextsComponent },
      { path: '', component: HomeComponent },
      { path: '**', component: HomeComponent }
    ])
  ],
  declarations: [ AppComponent, FooterComponent, AdminComponent, PrivacyComponent, HeaderComponent, NavbarComponent, HomeComponent, AboutComponent, EditorComponent, LoginComponent, SignupComponent, SubjectsComponent, TextsComponent],
  bootstrap:    [ AppComponent ]
})
export class AppModule { }
