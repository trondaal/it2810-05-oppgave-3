import { Component } from '@angular/core';

// Component will be used to display information about the website
@Component({
  selector: 'About',
  template:'<h1>coNotes is great!</h1>'

})
export class AboutComponent { }
