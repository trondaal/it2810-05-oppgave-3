import { Component } from '@angular/core';

// Component will be used as an admin panel for site administrators
@Component({
  selector: 'Admin',
  template:'<h1>Admin stuff</h1>'

})
export class AdminComponent { }
