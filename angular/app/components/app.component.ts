import { Component } from '@angular/core';

import {HeaderComponent} from './header.component';

import { FooterComponent } from './footer.component';
import { RouterModule }   from '@angular/router';

// Component is used to display the header and footer as static elements, while <router-outlet> defines what content is shown based on which link 
// that is clicked in the header or footer. Routes are defined in app.module.ts
@Component({
  selector: 'my-app',

  template: `
  <div class='wrapper'>
  <Header></Header>

  <div class='content'>
  	<router-outlet> </router-outlet>
  </div>
  <Footer></Footer>
  </div>`,
  styleUrls: ['app/css/app.css'],
})
export class AppComponent { }
