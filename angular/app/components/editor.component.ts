import { Component } from '@angular/core';

// Component will be used to display a text field which will be used when writing notes with multiple people. 
@Component({
  selector: 'Editor',
  template:'<h1>Editor</h1><textarea rows="10" cols="100"> </textarea> <button> Save </button>'

})
export class EditorComponent { }
