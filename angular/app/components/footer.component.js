"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var FooterComponent = (function () {
    function FooterComponent() {
    }
    FooterComponent = __decorate([
        core_1.Component({
            selector: 'Footer',
            template: "\n  <div class=\"root\" >\n  <div class=\"container\">\n  <span class=\"text\"> Gruppe 5 </span> <span class=\"spacer\">\u00B7</span>\n  <a routerLink=\"/\" class=\"link\"> Home </a> <span class=\"spacer\">\u00B7</span>\n  <a routerLink=\"/admin\" class=\"link\"> Admin </a> <span class=\"spacer\">\u00B7</span>\n  <a routerLink=\"/privacy\" class=\"link\">Privacy</a>\n  </div>\n  </div>\n  ",
            styleUrls: ['app/css/footer.css']
        }), 
        __metadata('design:paramtypes', [])
    ], FooterComponent);
    return FooterComponent;
}());
exports.FooterComponent = FooterComponent;
//# sourceMappingURL=footer.component.js.map