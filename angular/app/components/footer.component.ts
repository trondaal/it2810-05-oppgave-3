import { Component } from '@angular/core';

// Component is used to display the footer
@Component({
  selector: 'Footer',
  template: `
  <div class="root" >
  <div class="container">
  <span class="text"> Gruppe 5 </span> <span class="spacer">·</span>
  <a routerLink="/" class="link"> Home </a> <span class="spacer">·</span>
  <a routerLink="/admin" class="link"> Admin </a> <span class="spacer">·</span>
  <a routerLink="/privacy" class="link">Privacy</a>
  </div>
  </div>
  `,
  styleUrls:  ['app/css/footer.css']


})
export class FooterComponent { }
