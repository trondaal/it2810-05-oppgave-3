"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var HeaderComponent = (function () {
    function HeaderComponent() {
    }
    HeaderComponent = __decorate([
        core_1.Component({
            selector: 'Header',
            template: "\n\n\t  <div class=\"root\">\n\t  \n      <div class='container'>\n      <Navbar class='nav'> </Navbar>\n      <div class='brand'>\n      \t<img class='angularImage' src=\"app/images/angular.png\">\n      \t<p>Gruppe 5</p>\n      </div>\n        <div class='banner'>\n          <h1 class='bannerTitle'>coNotes</h1>\n          <p class='BannerDesc'>Sharing notes with your co-students made easy</p>\n\n        </div>\n      </div>\n    </div>\n\n    ",
            styleUrls: ['app/css/header.css'],
        }), 
        __metadata('design:paramtypes', [])
    ], HeaderComponent);
    return HeaderComponent;
}());
exports.HeaderComponent = HeaderComponent;
//# sourceMappingURL=header.component.js.map