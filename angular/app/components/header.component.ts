import { Component } from '@angular/core';
import { NavbarComponent } from './navbar.component'; // Imports navbar

// Component is used to display the header. The header imports the navbar and displayes it with the <Navbar>-tag.
	@Component({
	  selector: 'Header',
	  template:

	  `

	  <div class="root">
	  
      <div class='container'>
      <Navbar class='nav'> </Navbar>
      <div class='brand'>
      	<img class='angularImage' src="app/images/angular.png">
      	<p>Gruppe 5</p>
      </div>
        <div class='banner'>
          <h1 class='bannerTitle'>coNotes</h1>
          <p class='BannerDesc'>Sharing notes with your co-students made easy</p>

        </div>
      </div>
    </div>

    `,
	  styleUrls: ['app/css/header.css'],

	})


export class HeaderComponent { }