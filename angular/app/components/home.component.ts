import { Component } from '@angular/core';

// Component will be used to display the first page the user will see on the website 
@Component({
  selector: 'Home',
  template:'<h1>Home</h1>'

})
export class HomeComponent { }
