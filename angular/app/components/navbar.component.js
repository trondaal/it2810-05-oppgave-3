"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var NavbarComponent = (function () {
    function NavbarComponent() {
    }
    NavbarComponent = __decorate([
        core_1.Component({
            selector: 'Navbar',
            template: "\n  \t<div class='root' role=\"navigation\">\n \t\t<a class='link' routerLink=\"/\" routerLinkActive=\"active\">Home</a>\n \t\t<a class='link' routerLink=\"/about\" routerLinkActive=\"active\">About</a>\n \t\t<a class='link' routerLink=\"/editor\" routerLinkActive=\"active\">Editor</a>\n \t\t<span class='spacer'> | </span>\n \t\t<a class='link' routerLink=\"/subjects\" routerLinkActive=\"active\">Subjects</a>\n \t\t<a class='link' routerLink=\"/texts\" routerLinkActive=\"active\">Texts</a>\n \t\t<span class='spacer'> | </span>\n \t\t<a class='link' routerLink=\"/login\" routerLinkActive=\"active\">Log in</a>\n \t\t<span class='spacer'>or</span>\n \t\t<a class='link highlight' routerLink=\"/signup\" routerLinkActive=\"active\">Sign up</a>\n    </div>\n    ",
            styleUrls: ['app/css/navbar.css']
        }), 
        __metadata('design:paramtypes', [])
    ], NavbarComponent);
    return NavbarComponent;
}());
exports.NavbarComponent = NavbarComponent;
//# sourceMappingURL=navbar.component.js.map