import { Component } from '@angular/core';
import { RouterModule }   from '@angular/router';

// Component generates links used in the header component. 'routerLink acts as href in vanilla html but work for single page applications'
	@Component({
	  selector: 'Navbar',
	  template:
  	`
  	<div class='root' role="navigation">
 		<a class='link' routerLink="/" routerLinkActive="active">Home</a>
 		<a class='link' routerLink="/about" routerLinkActive="active">About</a>
 		<a class='link' routerLink="/editor" routerLinkActive="active">Editor</a>
 		<span class='spacer'> | </span>
 		<a class='link' routerLink="/subjects" routerLinkActive="active">Subjects</a>
 		<a class='link' routerLink="/texts" routerLinkActive="active">Texts</a>
 		<span class='spacer'> | </span>
 		<a class='link' routerLink="/login" routerLinkActive="active">Log in</a>
 		<span class='spacer'>or</span>
 		<a class='link highlight' routerLink="/signup" routerLinkActive="active">Sign up</a>
    </div>
    `,
	  styleUrls: ['app/css/navbar.css']

	})


export class NavbarComponent { }