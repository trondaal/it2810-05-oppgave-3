## About

Built using [React Starter Kit](https://www.reactstarterkit.com) as a base.

## Technologies and frameworks

* [React](https://facebook.github.io/react/)
* [Node.js](https://nodejs.org/)
* [Express](http://expressjs.com/) server
* [GraphQL](http://graphql.org/)
* [Webpack](http://webpack.github.io/)
* [Babel](http://babeljs.io/)
* [BrowserSync](http://www.browsersync.io/)
* [TinyMCE](https://www.tinymce.com/)

## Directory Layout

```
.
├── /build/                  # The folder for compiled output
├── /docs/                   # Documentation files for the project
├── /node_modules/           # 3rd-party libraries and utilities
├── /src/                    # The source code of the application
│   ├── /components/         # React components
│   ├── /content/            # Static pages
│   ├── /controllers/        # Contains controllers for db handling
│   ├── /core/               # Core framework and utility functions
│   ├── /data/               # MongoDB data folder
│   ├── /models/             # Database models
│   ├── /public/             # Static files which are copied into the /build/public folder
│   ├── /routes/             # Page/screen components along with the routing information
│   ├── /client.js           # Client-side startup script
│   ├── /config.js           # Global application settings
│   └── /server.js           # Server-side startup script
├── /test/                   # Unit and end-to-end tests
├── /tools/                  # Build automation scripts and utilities
│   ├── /lib/                # Library for utility snippets
│   ├── /build.js            # Builds the project from source to output (build) folder
│   ├── /bundle.js           # Bundles the web resources into package(s) through Webpack
│   ├── /clean.js            # Cleans up the output (build) folder
│   ├── /copy.js             # Copies static files to output (build) folder
│   ├── /deploy.js           # Deploys your web application
│   ├── /run.js              # Helper function for running build automation tasks
│   ├── /runServer.js        # Launches (or restarts) Node.js server
│   ├── /start.js            # Launches the development web server with "live reload"
│   └── /webpack.config.js   # Configurations for client-side and server-side bundles
└── package.json             # The list of 3rd party libraries and utilities
```

## Getting Started

### Requirements
  * [Node.js](https://nodejs.org/) v5.0 or newer
  * `npm` v3.3 or newer (new to [npm](https://docs.npmjs.com/)?)
  * `node-gyp` prerequisites mentioned [here](https://github.com/nodejs/node-gyp)

#### 1. Clone or pull latest version from bitbucket

#### 2. Run `npm install`

This will install both run-time project dependencies and developer tools listed
in [package.json](../package.json) file.

#### 3. Run `npm start` 

This command will build the app from the source files (`/src`) into the output
`/build` folder. As soon as the initial build completes, it will start the
Node.js server (`node build/server.js`) and [Browsersync](https://browsersync.io/)
with [HMR](https://webpack.github.io/docs/hot-module-replacement) on top of it.

> [http://localhost:3000/](http://localhost:3000/) — Node.js server (`build/server.js`)<br>
> [http://localhost:3000/graphql](http://localhost:3000/graphql) — GraphQL server and IDE<br>
> [http://localhost:3001/](http://localhost:3001/) — BrowserSync proxy with HMR, React Hot Transform<br>
> [http://localhost:3002/](http://localhost:3002/) — BrowserSync control panel (UI)

For a more detailed guide, go to docs/getting-started
