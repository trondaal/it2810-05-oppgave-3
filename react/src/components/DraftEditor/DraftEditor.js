import React from 'react';
import TinyMCE from 'react-tinymce';
import io from 'socket.io-client';
import $ from 'jquery';

var socket;
class DraftEditor extends React.Component {

  constructor(props){
    super(props);
    this.state={currentText: ''};

    //connect to correct socket pool
    this.socket = io.connect('/'+this.props.textName);
    //Get current content of the text
    var self = this;
    $.post('/getTextName', {name: this.props.textName}, function(res){
      console.log(res);
      self.setState({currentText: res.text});
    });


    console.log("StoredContent: ", this.state.currentText);

  }


  componentDidMount(){
    //Saves text every interval and emits changes to other clients
    var self = this;
    var intervalId = setInterval(function(){
      if(tinyMCE.get('textArea') != null){
          var content = tinyMCE.get('textArea').getContent();

	          console.log("_new_ CONTENT: ", content);
	          $.post('/saveExistingText', {name: self.props.textName, content: content}, function(res){
	            console.log(res);
	          });
	          console.log(content);
	          self.socket.emit(`textInput`, content);
	          self.socket.on(`push`, function(msg){
	            console.log(`Client Message: `, msg)
	            var text = tinyMCE.get('textArea').getContent();
	            tinyMCE.get('textArea').setContent(msg);
	            tinyMCE.activeEditor.focus();
	          });
        }
      }, 4000);
      this.setState({intervalId: intervalId});
  }
  //disconnect from socket pool & clears interval
  componentWillUnmount(){
    this.socket.disconnect();
    clearInterval(this.state.intervalId);
  }

  handleOnChange(){
    $('#textArea').keypress(function(event){
      console.log(event);
    })
  }


  render() {
    if(this.state.currentText){
      return (
        <TinyMCE id='textArea'
        content={this.state.currentText}
        config={{
          plugins: 'link image code',
            toolbar: 'undo redo | bold italic | fontselect fontsizeselect | alignleft aligncenter alignright | blockquote | bullist numlist | indent outdent | link | image | code'
        }}
        onKeyUp = {this.handleOnChange.bind(this)}
         />
      );
    }
    else{
      return (<p>Loading...</p>);
    }
  }
}

export default DraftEditor;
