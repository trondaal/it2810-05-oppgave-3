import React, { PropTypes } from 'react';
import SideNav, {MenuIcon} from 'react-simple-sidenav';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './Layout.css';
import Header from '../Header';
import Footer from '../Footer';

// Parent layout that renders the routes inside as children
function Layout({ children }) {
  return (
    <div>
      <Header />
      {React.Children.only(children)}
      <Footer />
    </div>
  );
}

Layout.propTypes = {
  children: PropTypes.element.isRequired,
};

export default withStyles(s)(Layout);
