import React from 'react';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './LoginCoNotes.css';
import $ from 'jquery';

// Login component we use. Has all methods and validation required here.

// Standard email validation regex
function validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}

class LoginCoNotes extends React.Component {

  constructor(props, context) {
    super(props, context);
    this.state= {email: '', password: '' };
    this.handleEmailChange = this.handleEmailChange.bind(this);
    this.handlePasswordChange = this.handlePasswordChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  	handleEmailChange(event) {
    this.setState({email: event.target.value});
  }

	handlePasswordChange(event) {
    this.setState({password: event.target.value});
  }

  handleSubmit(event) {
    event.preventDefault(); //This prevents the page from reloading when submitting the form
    if (this.state.email && this.state.password){ //checks that both field are filled in
      if (validateEmail(this.state.email)){
        $.post('/authorize', { email: this.state.email, password : this.state.password}, // Send an AJAX request to signup the new user
        function(returnedData){           //Callback function
          if (returnedData=="loginFail")  //The login attempt failed
            $('#feedback').text("Wrong password and/or username!");
          else //The login attempt was successfull
            $(location).attr('href', "/profile"); //Redirect to the profile page
        }).fail(function(){
              console.log("ajax error");
        });
       }
       else{
        $('#feedback').text("Please input a valid email");
       }
    }
    else{
      $('#feedback').text("Please fill out the entire form!");
    }
}



  render() {
    return (
    <div>
      <span id="feedback"> </span>
      <br />
      <form className={s.registerForm} onSubmit={this.handleSubmit} >
    	<input id="emailInput" className={s.input} type="text" placeholder="some@email.com" value={this.state.email} onChange={this.handleEmailChange} />
    	<br/>
    	<input id="pwInput" type="password" className={s.input} placeholder="Password" value={this.state.password} onChange={this.handlePasswordChange} />
    	<br/>
    	<button type="submit" onClick={this.handleSubmit} className={s.buttonInput} > Login </button>
      </form>
      <br/>

    </div>
    );
  }

}

export default withStyles(s)(LoginCoNotes);
