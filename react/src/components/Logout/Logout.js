import React from 'react';
import $ from 'jquery';
import cx from 'classnames';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './Logout.css';

// Logout component for properly logging the user out.

class Logout extends React.Component {

  constructor(props, context) {
    super(props, context);
    this.handleLogout = this.handleLogout.bind(this);
  }

  handleLogout(){
    $.post('/logout', null ,function(returnedData){
      $(location).attr('href', returnedData);
    }).fail(function(){
      console.log("ajax error");
    });
  }

  render(){
    return(
        <a className={cx(s.link, s.highlight)} onClick={this.handleLogout}>Logout</a>
    )
  }

}

export default withStyles(s)(Logout);
