import React, { PropTypes } from 'react';
import cx from 'classnames';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './Navigation.css';
import Link from '../Link';
import Logout from '../Logout/Logout'
import $ from 'jquery';

// Navigation component rendered as a navbar in the header.

class Navigation extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.state={email: ''};
  }


componentDidMount() {
  //Server request is used to verify that the user is logged in
    this.serverRequest = $.get('/userEmail', function (result) {
      this.setState({
        email: result
      });
    }.bind(this));
  }

  render(props) {

if(this.state.email){ //this will be rendered if the user is logged in
  return (
    <div className={cx(s.root, )} role="navigation">
      <Link className={s.link} to="/">Home</Link>
      <Link className={s.link} to="/about">About</Link>
      <Link className={s.link} to="/create">New Text</Link>
      <span className={s.spacer}> | </span>
      <Link className={s.link} to="/subjects">Subjects</Link>
      <Link className={s.link} to="/texts">Texts</Link>
      <span className={s.spacer}> | </span>
      <Link className={s.link} to="/profile">Profile</Link>
      <span className={s.spacer}>or</span>
      <Logout/>
      <span className={s.status}> You are currently logged in as: <Link to="/profile" className={s.link}> {this.state.email} </Link></span>

    </div>
  );
}
else{ //this will be rendered if the user is not logged in
  return (
      <div className={cx(s.root, )} role="navigation">
        <Link className={s.link} to="/">Home</Link>
        <Link className={s.link} to="/about">About</Link>
        <span className={s.spacer}> | </span>
        <Link className={s.link} to="/subjects">Subjects</Link>
        <span className={s.spacer}> | </span>
        <Link className={s.link} to="/login">Log In</Link>
        <span className={s.spacer}>or</span>
        <Link className={cx(s.link, s.highlight)} to="/register">Sign up</Link>
      </div>
    );
  }
}
}

Navigation.propTypes = {
  className: PropTypes.string,
};

export default withStyles(s)(Navigation);
