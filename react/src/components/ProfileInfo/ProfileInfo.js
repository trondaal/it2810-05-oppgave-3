import React from 'react';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './ProfileInfo.css';
import Link from '../Link';
import $ from 'jquery';

// This component has methods for filling and handling the profile page (Profile Route)

class ProfileInfo extends React.Component {

  constructor(props, context) {
    super(props, context);
    this.state={email: '', password: ''};
    this.handlePassInputChange = this.handlePassInputChange.bind(this);
    this.handlePasswordChange = this.handlePasswordChange.bind(this);
  }

  componentDidMount(){
    this.serverRequest = $.get('/userInfo', function (result) {
      this.setState({
        email: result.email
      });
    }.bind(this));
}

  handlePassInputChange(event){
    this.setState({password: event.target.value});
  }

  // Changes password of the user in the system

  handlePasswordChange(event){
    event.preventDefault();
    if ($('#pw').val() === $('#confirm').val()) {
      if (this.state.password){
        $.post('/changePassword', { email: this.state.email, password : this.state.password}, // Send an AJAX request to signup the new user
          function(returnedData){           //Callback function
            console.log("server answer: "+returnedData);
            if(returnedData=="changed"){
              alert("Password has been changed");
              $('#pw').val('');
              $('#confirm').val('');
            }
            else{
              alert("Something went wrong");
            }
          });
      }
      else{
        alert("Please input a password");}
    }

    else {
      alert("Password did not match")
    }
  }

  // Renders texts and courses you've contributed to
  render(props) {
    if (this.state.email){
    return (
    <div>
        <h2> Welcome, {this.state.email}! </h2>
        <h3>Change password: </h3>
        <table className={s.passForm}>
        <tr>
        	<td>
      			<label>New Password: </label>
      		</td>
      		<td>
      			<input value={this.state.password}  className={s.input} onChange={this.handlePassInputChange} type="text" id="pw" placeholder="New Password"/>
      		</td>
      	</tr>
      	<tr>
      		<td>
      			<label>Confirm Password: </label>
      		</td>
      		<td>
      			<input value={this.state.confirm}  className={s.input} type="text" id="confirm" placeholder="Confirm Password"/>
      		</td>
      	</tr>
    </table>
        <button className={s.saveBtn} onClick={this.handlePasswordChange}>Save</button>
        <br/>
        <br/>
        <Link to="/create">Click here to create a new text!</Link>
        <br/>
        <Link to="/texts">Click here to view your texts!</Link>
    </div>
    );
  }
else{
  return (
    <div>
        <span> You aren't logged in, what are you doing here? </span>
    </div>
    );
  }
}
}

export default withStyles(s)(ProfileInfo);
