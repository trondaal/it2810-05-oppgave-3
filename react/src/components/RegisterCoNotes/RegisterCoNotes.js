import React from 'react';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './RegisterCoNotes.css';
import $ from 'jquery';

// Registration component for the system

function validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}

class RegisterCoNotes extends React.Component {

  constructor(props, context) {
    super(props, context);
    this.state= {email: '', password: ''};
    this.handleEmailChange = this.handleEmailChange.bind(this);
    this.handlePasswordChange = this.handlePasswordChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  	handleEmailChange(event) {
    this.setState({email: event.target.value});
  }

	handlePasswordChange(event) {
    this.setState({password: event.target.value});
  }

  handleSubmit(event) {
    event.preventDefault();          //This prevents the page from reloading when submitting the form
    if (this.state.email && this.state.password) //checks that both field are filled in
      if (validateEmail(this.state.email)){
        $.post('/signup', { email: this.state.email, password : this.state.password}, // Send an AJAX request to signup the new user
        function(returnedData){        //Callback function
          if(returnedData=="alreadyExists") //The server replied that the user already existed
            $('#feedback').text("User already exists");
          else //The server replied that the user was created
            $(location).attr('href', "/login"); //redirect to login page
        }).fail(function(){
          console.log("ajax error");
        });
      }
      else{
        $('#feedback').text("Please input a valid email");
      }
    else{
      $('#feedback').text("Please fill out the entire form!");
    }
  }




  render() {
    return (
    <div>
      <span id="feedback"> </span>
      <br/>
      <form className={s.registerForm} onSubmit={this.handleSubmit} >
    	<input type="email" placeholder="some@email.com"  className={s.input} value={this.state.email} onChange={this.handleEmailChange} />
    	<br/>
    	<input type="password" placeholder="Password" className={s.input} value={this.state.password} onChange={this.handlePasswordChange} />
    	<br/>
    	<button type="submit" className={s.buttonInput} onClick={this.handleSubmit}> Register </button>
      </form>
      <br/>
    </div>
    );
  }

}

export default withStyles(s)(RegisterCoNotes);
