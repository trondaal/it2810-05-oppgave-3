import React, {Component, PropTypes} from 'react';
import io from 'socket.io-client'
var socket = io.connect('http://localhost:3000/');

// SocketIO component that handles synchronization between user clients

class Socket extends Component {
  constructor () {
    super()

    socket.on(`server:event`, data => {
      this.setState({ data })
    })
  }

  sendMessage = message => {
    socket.emit(`chat`, message)
  }

  render () {
    return (
      <Child
        socket = { socket }
        sendMessage = { this.sendMessage }
      />
    )
  }
}
