import React from 'react';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './SubjectEntry.css';
import $ from 'jquery';
import Link from '../Link';

var texts = {   //dummy data to initiate
   "subject1":{
      "title":"AI1",
      "faculty":"IME"
   }
};
var arr = Object.keys(texts).map(function(k) { return texts[k] }); //converts to array for react usage.

class SubjectEntry extends React.Component {

  constructor(props, context) {
    super(props, context);
    this.state= {email: '', data: '', open: '+'};
    this.handleClick = this.handleClick.bind(this);
  }

  componentWillReceiveProps(){
    this.setState({data: ''});
  }

  handleClick(event){


      if (this.state.data){
        this.setState({data: '', open: '+'});
      }
      else{
      this.serverRequest = $.post('/getSubjectTexts', {subject: this.props.title}, function (result) {
        this.setState({
          data: result
        });
        this.setState({open: '-'});
      }.bind(this));

    }
  }


  render() {
    if (this.state.data){
      return (
      <div className={s.rootOpen}>
      <tr>
        <td>
          <span className={s.courseLabel}>Course: </span>
        </td>
        <td>
          <span className={s.courseTitle}> {this.props.title} </span>
        </td>
      </tr>
      <tr>
        <td>
           <span className={s.facultyLabel}> Faculty: </span>
        </td>
        <td>
          <span className={s.facultyTitle}>  {this.props.faculty} </span><button onClick={this.handleClick}> + </button>
        </td>
      </tr>



        <div id="textsEx">
            <ul>
            {
            this.state.data.map(function(text, index){
              return <Link to={'/editor?name='+text.name}><li className={s.entry}> {text.name}</li></Link>
            })
            }
            </ul>
        </div>

      </div>

      );
  }
    else{
    return (
      <div className={s.rootClosed}>
           <tr>
        <td>
          <span className={s.courseLabel}>Course: </span>
        </td>
        <td>
          <span className={s.courseTitle} >  {this.props.title}  </span>
        </td>
      </tr>
      <tr>
        <td>
           <span className={s.facultyLabel}> Faculty: </span>
        </td>
        <td>
          <span className={s.facultyTitle}>  {this.props.faculty} </span><button onClick={this.handleClick}> + </button>
        </td>
      </tr>

      </div>
      );
    }
  }

}

export default withStyles(s)(SubjectEntry);
