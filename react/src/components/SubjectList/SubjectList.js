import React from 'react';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './SubjectList.css';
import $ from 'jquery';
import SubjectEntry from '../SubjectEntry';
import LazyLoad from 'react-lazy-load';

var startData = { //dummy data to initiate
   "subject1":{
      "title":"Currently loading",
      "faculty":"Please wait..."
   }
};
var arr = Object.keys(startData).map(function(k) { return startData[k] }); //converts to array for react usage.

function compare(a,b) {
  if (a[0].toUpperCase() < b[0].toUpperCase())
    return -1;
  if (a[0].toUpperCase() > b[0].toUpperCase())
    return 1;
  return 0;
}
function otherCompare(a,b) {
  if (a[0].toUpperCase() > b[0].toUpperCase())
    return -1;
  if (a[0].toUpperCase() < b[0].toUpperCase())
    return 1;
  return 0;
}
function compareF(a,b) {
  if (a[1].toUpperCase() < b[1].toUpperCase())
    return -1;
  if (a[1].toUpperCase() > b[1].toUpperCase())
    return 1;
  return 0;
}
function otherCompareF(a,b) {
  if (a[1].toUpperCase() > b[1].toUpperCase())
    return -1;
  if (a[1].toUpperCase() < b[1].toUpperCase())
    return 1;
  return 0;
}


class SubjectList extends React.Component {

  constructor(props, context) {
    super(props, context);
    this.state= {email: '', data: arr, allData: arr, cSorted: '0', fSorted: '0', cFilter: '', fFilter: '', courseSortButton: 'Sort By Course', facultySortButton:'Sort By Faculty'}; //cSorted->sorted by course
    this.courseSort = this.courseSort.bind(this);
    this.facultySort = this.facultySort.bind(this);
    this.handleCourseFilter = this.handleCourseFilter.bind(this);
    this.handleFacultyFilter = this.handleFacultyFilter.bind(this);
  }

  courseSort(){
    console.log(this.props.children);
    if (this.state.cSorted=='0' || this.state.cSorted=='2' ){
      var sorted = this.state.data.sort(compare);
      this.setState({data : sorted, cSorted:'1', fSorted:'0', courseSortButton: 'Sort By Course ↓', facultySortButton: 'Sort By Faculty'});
    }
    else if (this.state.cSorted=='1'){
      var sorted = this.state.data.sort(otherCompare);
      this.setState({data : sorted, cSorted:'2', fSorted:'0', courseSortButton: 'Sort By Course ↑', facultySortButton: 'Sort By Faculty'});
    }
  }

  facultySort(){
    if (this.state.fSorted=='0' || this.state.fSorted=='2' ){
      var sorted = this.state.data.sort(compareF);
      this.setState({data : sorted, fSorted:'1', cSorted:'0', courseSortButton: 'Sort By Course', facultySortButton: 'Sort By Faculty ↓'});
    }
    else if (this.state.fSorted=='1'){
      var sorted = this.state.data.sort(otherCompareF);
      this.setState({data : sorted, fSorted:'2', cSorted:'0', courseSortButton: 'Sort By Course', facultySortButton: 'Sort By Faculty ↑'});
    }
  }

handleFacultyFilter(event){
    var newData = [];
    var query = event.target.value;
    this.setState({fFilter: query, cFilter: ''});
    for (var i=0; i<this.state.allData.length; i++){
      if(this.state.allData[i][1].toUpperCase().indexOf(query.toUpperCase()) !== -1){
        newData.push(this.state.allData[i]);
      }
    }
    if (this.state.fSorted=='1') newData.sort(compareF);
    if (this.state.fSorted=='2') newData.sort(otherCompareF);
    if (this.state.cSorted=='1') newData.sort(compare);
    if (this.state.cSorted=='2') newData.sort(otherCompare);
    if (newData.length==0)newData.push(["No faculties correspond to the filer!",""]);
    this.setState({data: newData});
  }

handleCourseFilter(event){
    var newData = [];
    var query = event.target.value;
    this.setState({cFilter: query, fFilter: ''});
    for (var i=0; i<this.state.allData.length; i++){
      if(this.state.allData[i][0].toUpperCase().indexOf(query.toUpperCase()) !== -1){
        newData.push(this.state.allData[i]);
      }
    }
    if (this.state.fSorted=='1') newData.sort(compareF);
    if (this.state.fSorted=='2') newData.sort(otherCompareF);
    if (this.state.cSorted=='1') newData.sort(compare);
    if (this.state.cSorted=='2') newData.sort(otherCompare);
    if (newData.length==0)newData.push(["No courses correspond to the filer!",""]);
    this.setState({data: newData});
  }


  componentDidMount(){
      this.serverRequest = $.get('/userEmail', function (result) {
      this.setState({
        email: result
      });
    }.bind(this));

    this.serverRequest2 = $.get('/getAllCourses', function (result) {
      console.log("Result[0] ",result[0][0]);
      this.setState({
        data: result,
        allData: result
      });
    }.bind(this));

  }

  render() {
    if (this.state.email){

    return (

    <div>
    <h1 className={s.title}> Subjects </h1>
    <a href="#name">Link to text-distribution chart</a><br/>
    <span> This page shows an overview of all the courses that has been created by <b>all users</b>.<br/>
          You can press the "<b>+</b>" button to see the texts <b>you</b> have created for a given course.<br/>
          If a course list is empty, you do not have any texts in that course<br/><br/>
          You can use the textfields below to filter the courses by name or faculty. You can also sort the list using the corresponding buttons<br/>
    </span>

    <table>
    <tr>
      <td>
      <input type="text" onChange={this.handleCourseFilter} value={this.state.cFilter} placeholder="CourseFilter" />
      </td>
      <td>
      <input type="text" onChange={this.handleFacultyFilter} value={this.state.fFilter} placeholder="FacultyFilter" />
      </td>
    </tr>
    <tr>
      <td>
      <button className={s.sortBtn} onClick={this.courseSort}>{this.state.courseSortButton}</button>

      </td>
      <td>
      <button className={s.sortBtn} onClick={this.facultySort}>{this.state.facultySortButton}</button>
      </td>
    </tr>
    </table>

    <table className={s.list}>

    {
      this.state.data.map(function(subject, index){ 	//this loops over the data-object, and generates SubjectEntry for each course
         return <SubjectEntry  title={subject[0]} faculty={subject[1]}/>

      })
    }
    </table>

    </div>
    );
}
else
{
    return (
    <div>
    <h2> Please log in to access the complete overview of subjects!</h2>
    </div>
    );
}

  }
}

export default withStyles(s)(SubjectList);
