import React from 'react';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './TextList.css';
import $ from 'jquery';
import Link from '../Link';

class TextList extends React.Component {

  constructor(props, context) {
    super(props, context);
    this.state= {email: '', data: ''}; //cSorted->sorted by course
  }

  componentDidMount(){
    this.setState({data: ['you have no texts']});
      this.serverRequest = $.get('/userEmail', function (result) {
      this.setState({
        email: result
      });
      if(result)
      this.serverRequest2 = $.post('/getUserTexts', function (result) {
        if (result){
          this.setState({
            data: result
          });
       }
    }.bind(this));
    }.bind(this));


  }

  render() {
    if (this.state.email){
    return (
    <div>
    <h2> Your texts: </h2>
    <span> This page displays all your texts, if any: </span>
    <ul className={s.liist}>
    {
      this.state.data.map(function(text, index){
         return <Link className={s.link} to={'/editor?name='+text}><li>⚫ {text}</li></Link>

      })
    }
    <br/>
    <Link to="/create">Create new text</Link>
    </ul>
    </div>
    );
}
else
{
    return (
    <div>
      If you log in you will see your texts here!

    </div>
    );
}

  }
}

export default withStyles(s)(TextList);
