import {Chart} from 'react-google-charts';
import React from 'react';
import $ from 'jquery';

// Provides a chart view over the different courses that has lecture notes

class SubjectChart extends React.Component {
  constructor(props){
    super(props);

    this.state={
      data:[["Subjects","Documents avaliable"],["Math",11],["English",2],["Gymnastics",2],["Physics",2],["Geography",7]],
      options: {"title":"","pieHole":0.4,"is3D":true}}
    };

//sends a get.request to the database to fill out the graph
  componentDidMount(){
    console.log("I did mount yay!");
    this.serverRequest = $.get('/getChartData', function (result) {
      this.setState({
        data: result
      })
    }.bind(this));
  }

//renders size and assigns names

  render() {
    return (
      <Chart
        chartType="PieChart"
        graph_id="PieChart"
        width="100%"
        height="400px"
        data={this.state.data}
        options={this.state.options}
        legend_toggle
       />
    );
  }
};
export default SubjectChart;
