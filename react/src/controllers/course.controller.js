var Course = require('../models/course.model');
var crypto = require('crypto');


//Adds a text to course, creates course if not existing
export function addTextToCourse(courseName, facultyName, TextName, fn) {
  //Finds course on name and faculty
  Course.update({
    name: courseName,
    faculty: facultyName
  }, {
    //Adds text to course's array of texts
    $addToSet: {
      texts: [TextName]
    }
  }, {
    upsert: true
  }, function(err, course) {
    if (err) {
      return fn(err, null);
    } else {
      return fn(null, course)
    }
  });
}


// Get all courses
export function getAll(callback){
  Course.find({}, function(err, courses){
    if(err){
      callback(err, null);
    }else{
    callback(null,courses);
    }
  });
}

// Helping function for returning data in custom array to be used in graphic chart
export function getChartData(callback){
  Course.find({}, function(err, courses){
    if(err){
      callback(err, null);
    }
    else{
      //If courses found, adds course and amount of texts corresponding to course to custom array
      var chartArray = [["Subjects","Documents avaliable"]];
      courses.forEach(function(entry){
        var partArray = [entry.name, entry.texts.length];
        chartArray.push(partArray);
      });
      callback(null, chartArray);
    }
  });
}

