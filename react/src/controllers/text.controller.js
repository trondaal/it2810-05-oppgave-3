var Text = require('../models/text.model');


//Save texts
export function saveText(text, fn) {

  text.text = 'Start Writing!';
  var textEntry = new Text(text);
  textEntry.save(function(err) {
    if (err) {
      return fn(new Error(err), null);
    } else {
      return fn(null, textEntry);
    }
  });

}


// Takes in an array of texts from user, and a subject

export function saveExistingText(name, content, fn){
  Text.update({
    name: name,
  },{text: content}, function(err, course) {
    if (err) {
      return fn(err, null);
    } else {
      return fn(null, course)
    }
  });
}



export function userTextsBySubject(txtArray, subject, callback){
  var returnArray = [];
  // Iterates through the given textArray
  txtArray.forEach(function(entry){
    // Gets a text Object
    getTextOnId(entry, function(err, txt){
      if(txt){
        // If the text's course corresponds to the given subject, pushes the text Object into the returnArray
        if(txt.course == subject){
          returnArray.push(txt);
        }
      }
    });
  });
  // Timeout to avoid async issues, uses a timeout, if not it will return an empty returnArray
  setTimeout(function(){return callback(null,returnArray);}, 100);

}


// Find a text Object on text name
export function getTextOnId(id, callback){
  Text.findOne({
    name: id
  },
  function(err, text){
    return callback(null,text);
  })

}
