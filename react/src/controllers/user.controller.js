var User = require('../models/user.model');
var crypto = require('crypto');

// Hjelpefunksjoner/////////////////////////////////////////////////////////////////////////////////////////////////////

// Hashfunction to avoid saving passwords in pure text
function hash(pwd, salt, fn) {
  // If using predefined salt as argument or not
  if (3 == arguments.length) {
    crypto.pbkdf2(pwd, salt, 12000, 128, 'sha512', fn);
  } else {
    fn = salt;
    crypto.randomBytes(128, function(err, salt){
      if (err) return fn(err);
      // If not using predefined salt, creates one in base64
      salt = salt.toString('base64');
      crypto.pbkdf2(pwd, salt, 12000, 128, 'sha512', function(err, hash){
        if (err) return fn(err);
        fn(null, salt, hash);
      });
    });
  }
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////




// Register function
export function register(email, pass, fn){
  // Checks if user on email already exists
  User.count({
    email: email
  },function(err,count) {
    if (count == 0){
      // If success, hashes password
      hash(pass, function (err, salt, hash) {
        if (err) return fn(new Error(err));
        // Creates new mongoDB document and saves it
        var user = new User({
          email: email,
          salt: salt,
          hash: hash
        }).save(function(err){
          if(err){
            return fn(new Error(err));
          }
          else{
            // If user created and saved, returns the user to callback function
            return fn(null, user);
          }
        });
      });
    }
    else{
      return fn(new Error('User already exists'));
    }
  });
}

// Function for changing password
export function changePassword(email, pass, fn){
  //Hashes the password
  hash(pass, function (err, salt, hash){
    if(err) return fn(new Error(err));
    //Updates
    User.update({email: email}, {salt: salt, hash: hash}, function(err){
      if(err)
        return fn(err,null);
      else{
        return fn(null,"changed");
      }
    });
  });
}



// Login function
export function login(email, pass, fn) {
  // Checks if a user with corresponding email exists in database
  User.findOne({
      email: email
    },
    function (err, user) {
      if (user) {
        if (err) return fn(new Error('cannot find user'));
        // If success, hashes password with the user salt and compares the hashes
        hash(pass, user.salt, function (err, hash) {
          if (err) return fn(err);
          // If hashes are equal, the passwords are equal and user logs in
          if (hash == user.hash) return fn(null, user);
          fn(new Error('invalid password'));
        });
      } else {
        return fn(new Error('cannot find user'));
      }
    });
}

// Find user on email
export function getInfo(email,fn) {
  User.findOne({
      email: email
    },
    function (err, user) {
        return fn(null ,user);
    });
}

// Find user texts on email, returns as Array
export function getTexts(email,fn){
  User.findOne({
      email: email
    },
    function (err, user) {
        return fn(null ,user.texts);
    });
}


// Adding text to the user texts array
export function addTextToUser(email, TextName, fn) {
  User.update({
    email: email
  },
  {
    $addToSet: {
      texts: [TextName]
    }
  },
  {
    upsert: true
  },
  function(err, user) {
    if (err) {
      return fn(err, null);
    } else {
      return fn(null, user);
    }
  });
}
