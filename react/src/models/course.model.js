import mongoose from 'mongoose';
import Text from './text.model';

var Schema = mongoose.Schema;
// Subjectname and corresponding faculty, texts is an array of belonging texts
var CourseSchema = new Schema({
  name: {
    type: String,
    unique: true,
  },
  faculty: String,
  texts: [String]
});

module.exports = mongoose.model('Course', CourseSchema);
