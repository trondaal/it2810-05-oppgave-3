import mongoose from 'mongoose';
import Course from './course.model'

var Schema = mongoose.Schema;

//Name of the text, text is the actual content, course is belonging subject, creator is the user who created the text
//Timestamp is when the text was created, collaborators is for noncreator users who have access to the text
var TextSchema = new Schema({
  name: {
    type: String,
    index:
      {
        unique: true
      },
    required: true
  },
  text: String,
  course: String,
  creator: String,
  timestamp: {
    type: Date,
    default: Date.now
  },
  collaborators: [String],


});

module.exports = mongoose.model('Text', TextSchema);
