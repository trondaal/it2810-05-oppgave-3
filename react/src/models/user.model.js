import mongoose from 'mongoose';
var Schema = mongoose.Schema;

//salt and hash are used for password storing, password is never stored in pure text
var UserSchema = new mongoose.Schema({
  email: String,
  salt: String,
  hash: String,
  texts: [String]
});

module.exports = mongoose.model('users', UserSchema);
