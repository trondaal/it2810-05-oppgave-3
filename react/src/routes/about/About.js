import React, { PropTypes } from 'react';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import Layout from '../../components/Layout';
import s from './About.css';

// Simple about page for coNotes

function About({ title }) {
  return (
    <Layout>
      <div className={s.root}>
        <div className={s.container}>
          <h1> About coNotes</h1>
          <p>The concept of coNotes is that students can share the workload that is taking
          notes from class. By collaborating, the students will get the notes written
          down quicker and will as a consequence be able to pay more attention to the
          lecturer. With coNotes the created documents will be stored in our database
          and the creators of those documents will be able to reach them wherever and
          whenever.</p>
          <h1>Get started!</h1>
          <p>To begin using the service, you have to sign up using email.
            After an account is created. There are three options to chose
          from. Either you can browse subjects, get a list of all documents you have
          access to, or you can start editing a new project to share with your school mates. </p>

        </div>
      </div>
    </Layout>
  );
}

About.propTypes = {
  title: PropTypes.string.isRequired,
};

export default withStyles(s)(About);
