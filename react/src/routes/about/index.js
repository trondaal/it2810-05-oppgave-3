import React from 'react';
import About from './About';

const title = 'About us';

export default {

  path: '/about',

  async action() {
    return {
      title,
      component: <About title={title} />
    };
  },

};
