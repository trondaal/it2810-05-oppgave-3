import React, {PropTypes} from 'react';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import Layout from '../../components/Layout';
import Link from '../../components/Link';
import s from './CreateText.css';
import $ from 'jquery';

// Text overview page where you navigate through the data structure to create a new lecture note

function capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}

function compare(a,b) {
  if (a < b)
    return -1;
  if (a > b)
    return 1;
  return 0;
}
class CreateText extends React.Component {

  constructor(props, context) {
    super(props, context);
    this.state = {name: '', course: '', faculty: '', facultyText:'', email: '', disabled:'false', text: 'Start Writing!', courseData: '', listData: ''};
    this.handleNameChange = this.handleNameChange.bind(this);
    this.handleCourseChange = this.handleCourseChange.bind(this);
    this.handleFacultyChange = this.handleFacultyChange.bind(this);
    this.handleFacultyTextChange = this.handleFacultyTextChange.bind(this);
  }

  componentDidMount() {
    this.serverRequest = $.get('/userEmail', function (result) {
      this.setState({
        email: result
      });
    }.bind(this));

    //retrieves all courses, used to check if the input course already exist
    this.serverRequest2 = $.get('/getAllCourses', function (result) {
      var value = result;
      var uniqueCourses = ["HF","DMF","AB","IDI","IME","IVT","NT","SVT","FHS","FLT","FT","HHIT"];
      $.each(value, function(i, el){
          if($.inArray(el[1], uniqueCourses) === -1) uniqueCourses.push(el[1]);
      });
      uniqueCourses = uniqueCourses.sort(compare);
      this.setState({
        courseData: uniqueCourses,
        listData: value
      });
    }.bind(this));
  }

  handleNameChange(event) {
    this.setState({name: event.target.value});
  }

  handleFacultyTextChange(event){
    var value = event.target.value;
    if (value.length>0){
      this.setState({faculty: 'Other', facultyText: value});
      $('#facSelect').prop("disabled",true);
    }
    else{
      this.setState({facultyText: value, faculty: 'IME'});
      $('#facSelect').prop("disabled",false);
    }

  }

  handleCourseChange(event) {
    var value = event.target.value;
    this.setState({course: capitalizeFirstLetter(value)});
    $('#facSelect').prop("disabled",false);
    $('#facInput').prop("disabled",false);
    $('#facInput').prop("placeholder","Faculty");
    for (var i=0; i<this.state.listData.length; i++){
      if (value.toUpperCase()==this.state.listData[i][0].toUpperCase()){
        this.setState({faculty: this.state.listData[i][1], facultyText:'', course:this.state.listData[i][0]});
        $('#facSelect').prop("disabled",true);
         $('#facInput').prop("disabled",true);
         $('#facInput').prop("placeholder","");

      }
    }
  }

  handleFacultyChange(event) {
    this.setState({faculty: event.target.value, facultyText:''});
  }

  createText(event) {
    if (this.state.facultyText){
          if (this.state.name && this.state.course) {
      $.post('/createText', {
          name: this.state.name,
          course: this.state.course,
          faculty: this.state.facultyText,
          text: this.state.text
        },
        function (returnedData) {
          console.log("ERROR: " + returnedData);
        });
      var loc = "/editor?name=" + this.state.name;
      $(location).attr('href', loc);
    }
    else {
      alert("Please input all fields");
    }
    }
    else if (this.state.faculty){
    if (this.state.name && this.state.course) {
      $.post('/createText', {
          name: this.state.name,
          course: this.state.course,
          faculty: this.state.faculty,
          text: this.state.text
        },
        function (returnedData) {
          console.log("ERROR: " + returnedData);
        });
      var loc = "/editor?name=" + this.state.name;
      $(location).attr('href', loc);
    }
    else {
      alert("Please input all fields");
    }
    }

  }

  createCourse(event) {
    $.post('/createCourse', {course: this.state.course},
      function (returnedData) {
        console.log("ERROR: " + returnedData);
      });
  }

  render() {
    if (this.state.email) {
      if (this.state.courseData){
      return (
        <Layout>
          <div className={s.root}>
            <div className={s.container}>
              <h2> Create a new text! </h2>
              {/* To create new text */}
              <form className='form-horizontal'>
                <div className="form-group">
                <p> Please input text title, course title, and select(or input) the corresponding faculty </p>
                  <table>
                    <tr>
                      <td>
                        <span>Text title:</span>
                      </td>
                      <td>
                        <input className='form-control' type="name" value={this.state.name}
                               onChange={this.handleNameChange} placeholder="Enter name"/>
                      </td>
                    </tr>
                    <tr>
                      <td>
                        <span>Course title: </span>
                      </td>
                      <td>
                        <input className='form-control' type="course" value={this.state.course}
                               onChange={this.handleCourseChange} placeholder="Enter course"/>
                      </td>

                    </tr>
                    <tr>
                      <td>
                        <span>Faculty title </span>
                      </td>
                      <td>
                        <select id="facSelect" value={this.state.faculty} onChange={this.handleFacultyChange}>

                          {
                            this.state.courseData.map(function(subject, index){   //this loops over the data-object, and generates SubjectEntry for each course
                               return <option value={subject}>{subject}</option>

                            })
                          }
                          <option value="Other">Other:</option>
                        </select>
                        <input type="text" id="facInput" className={s.facInput}  placeholder="Faculty" value={this.state.facultyText} onChange={this.handleFacultyTextChange} />
                    </td>
                    </tr>
  </table>
                   <span> If the correct faculty is not present in the list, fill in the textfield</span>
                    <br/>
                        <button className="btn btn-default" id='submitBtn' type='button'
                                onClick={this.createText.bind(this)}>Submit
                        </button>


                </div>
              </form>
            </div>
          </div>
        </Layout>
      );
    }
    else{
      return (
        <Layout>
          <div className={s.root}>
            <div className={s.container}>
              <h2> Create a new text! </h2>
              {/* To create new text */}
              <form className='form-horizontal'>
                <div className="form-group">
                  <table>
                    <tr>
                      <td>
                        <span>Text title:</span>
                      </td>
                      <td>
                        <input className='form-control' type="name" value={this.state.name}
                               onChange={this.handleNameChange} placeholder="Enter name"/>
                      </td>
                    </tr>
                    <tr>
                      <td>
                        <span>Course title: </span>
                      </td>
                      <td>
                        <input className='form-control' type="course" value={this.state.course}
                               onChange={this.handleCourseChange} placeholder="Enter course"/>
                      </td>

                    </tr>
                    <tr>
                      <td>
                        <span>Faculty title </span>
                      </td>
                      <td>
                        <select id="facSelect" value={this.state.faculty} onChange={this.handleFacultyChange}>
                          <option value="HF">HF</option>
                          <option value="DMF">DMF</option>
                          <option value="AB">AB</option>
                          <option value="IDI">IDI</option>
                          <option value="IME">IME</option>
                          <option value="IVT">IVT</option>
                          <option value="NT">NT</option>
                          <option value="SVT">SVT</option>
                          <option value="FHS">FHS</option>
                          <option value="FLT">FLT</option>
                          <option value="FT">FT</option>
                          <option value="HHIT">HHIT</option>
                          <option value="Other">Other:</option>
                        </select>
                        <input type="text" id="facInput" className={s.facInput} placeholder="Faculty" value={this.state.facultyText} onChange={this.handleFacultyTextChange} />

                      </td>
                    </tr>
                    </table>
                   <span> If the correct faculty is not present in the list, fill in the textfield</span>
                    <br/>
                        <button className="btn btn-default" id='submitBtn' type='button'
                                onClick={this.createText.bind(this)}>Submit
                        </button>
                </div>
              </form>
            </div>
          </div>
        </Layout>
      );

    }
  }
    else {
      return (
        <Layout>
          <div className={s.root}>
            <div className={s.container}>
              <h2> Please <Link to="/login">log in</Link> to create texts </h2>
            </div>
          </div>
        </Layout>
      );

    }
  }
}


// <DraftEditor /> : this is the new text editor. Add it above to render it when it works


export default withStyles(s)(CreateText);
