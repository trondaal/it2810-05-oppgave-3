import React from 'react';
import CreateText from './CreateText'

const title = 'CreateText';

export default {

  path: '/create',

  async action() {
    return {
      title,
      component: <CreateText title={title} />
    };
  },

};
