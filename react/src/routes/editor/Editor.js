import React, { PropTypes } from 'react';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import Layout from '../../components/Layout';
import DraftEditor from '../../components/DraftEditor/DraftEditor';
import s from './Editor.css';
import $ from 'jquery';

// Editor page with the TinyMCE component. Uses DraftEditor component as a core

class Editor extends React.Component {
  constructor(props){
    super(props);
    this.state = {currentText: ''};

  }

  componentDidMount(){
    //Gets the text param from url
    var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
  };


  this.setState({
    currentText: getUrlParameter('name')
  });

  console.log("State: ", this.state.currentText);
  /*
  $.post('/textName', {name: this.state.currentText}, function(res){
    if(res){
      console.log(res);
    }
    else{
      $(location).attr('href', '/');
    }
  });
  */

  }

  render(){

    if(this.state.currentText){
      return (
        <Layout>

          <div className={s.root}>
            <div className={s.container}>
              <DraftEditor textName={this.state.currentText} />
            </div>

          </div>

        </Layout>
      );
    }
    else{
      return(<Layout>

        <div className={s.root}>
          <div className={s.container}>
            <p>Loading...</p>
          </div>

        </div>

      </Layout>);
    }
  }
}



// <DraftEditor /> : this is the new text editor. Add it above to render it when it works



export default withStyles(s)(Editor);
