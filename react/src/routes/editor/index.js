import React from 'react';
import Editor from './Editor'

const title = 'Editor';

export default {

  path: '/editor',

  async action() {
    return {
      title,
      component: <Editor title={title} />
    };
  },

};
