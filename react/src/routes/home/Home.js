import React, { PropTypes } from 'react';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import Layout from '../../components/Layout';
import s from './Home.css';
import documents from './documents.png';

// Home route and landing page

function Home() {
  return (
    <Layout>
      <div className={s.root}>
        <div className={s.container}>
          <h1 className={s.title}>Welcome to coNotes</h1>
          <h3>The web-based application in which students can work together by creating,
          editing and storing their lecture notes. This platform allows students to take
          faster and better notes that won't crumble up in the backpacks on their way home.
          </h3>
        </div>
          <div className={s.picture}>
            <img src={documents} width="200" height="200" alt="gruppe5" />
          </div>
      </div>
    </Layout>
  );
}

export default withStyles(s)(Home);
