import React from 'react';
import Home from './Home';
import fetch from '../../core/fetch';

export default {

  path: '/',

  async action() {
    return {
      title: 'Home',
      component: <Home />,
    };
  },

};
