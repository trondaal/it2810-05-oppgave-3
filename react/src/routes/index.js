/* eslint-disable global-require */

// The top-level (parent) route
export default {


  path: '/',



  // Keep in mind, routes are evaluated in order
  // Routes are defined below. To add something to the route, require it below and link to it.
  children: [

    require('./home').default,
    require('./login').default,
    require('./register').default,
    require('./createText').default,
    require('./editor').default,
    require('./about').default,
    require('./texts').default,
    require('./subjects').default,
    require('./privacy').default,

    require('./profile').default,

    // place new routes before...
    require('./notFound').default,
  ],

  // Routing function
  async action({ next }) {
    let route;

    // Execute each child route until one of them return the result
    // TODO: move this logic to the `next` function
    do {
      route = await next();
    } while (!route);

    // Provide default values for title, description etc.
    route.title = `${route.title || 'Untitled Page'}`;
    route.description = route.description || '';

    return route;
  },

};
