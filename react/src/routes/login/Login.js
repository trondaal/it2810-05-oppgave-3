import React, { PropTypes } from 'react';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import Layout from '../../components/Layout';
import LoginCoNotes from '../../components/LoginCoNotes/LoginCoNotes';
import s from './Login.css';
import Link from '../../components/Link';

// Basic login page with directions to register

function Login({ title }) {
  return (
    <Layout>
      <div className={s.root}>
        <div className={s.container}>
          <h1>{title}</h1>
          <LoginCoNotes/>
          Don't have a user? <Link to="/register">Sign Up </Link>
        </div>
      </div>
    </Layout>
  );
}

Login.propTypes = {
  title: PropTypes.string.isRequired,
};

export default withStyles(s)(Login);
