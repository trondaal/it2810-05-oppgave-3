import React, { PropTypes } from 'react';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import Layout from '../../components/Layout';
import s from './Privacy.css';

// Short explanation about our privacy policy

function Privacy({ title }) {
  return (
    <Layout>
      <div className={s.root}>
        <div className={s.container}>
          <h1>coNotes Privacy Policy</h1>
          <p>This Privacy Policy was last modified on 02.11.2016</p>
          <p>This page informs you of our policies regarding the collection, use and disclosure of Personal Information we recive from users of this Site.</p>
          <p>We use your Personal Information only for providing and improving the site. By using the site, you agree to the collection and use of information in accordance with this policy.</p>
          <h2>Information Collection And Use</h2>
          <p>While using our Site, we may ask you to provide us with certain personal information that can be used to contact or identify you. Personally identifiable information may include, but is not limited to your name.</p>
          <h2>Log Data</h2>
          <p>Like many site operators, we might collect information that your browser sends whenever you visit our site. This Log Data may include information such as your computer's Internet Protocol address, browser type, browser version, the date and time you visit, and other statistics.</p>
        </div>
      </div>
    </Layout>
  );
}

Privacy.propTypes = {
  title: PropTypes.string.isRequired,
};

export default withStyles(s)(Privacy);
