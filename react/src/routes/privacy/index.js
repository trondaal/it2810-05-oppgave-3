import React from 'react';
import Privacy from './Privacy';

const title = 'Privacy';

export default {

  path: '/privacy',

  async action() {
    return {
      title,
      component: <Privacy title={title} />
    };
  },

};
