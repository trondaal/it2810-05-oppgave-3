//profile
import React, { PropTypes } from 'react';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import Layout from '../../components/Layout';
import ProfileInfo from '../../components/ProfileInfo';
import s from './Profile.css';
import SubjectList from '../../components/SubjectList/SubjectList';



// Profile route that is expanded by profileinfo component

function Profile({ title }) {
  return (
    <Layout>
      <div className={s.root}>
        <div className={s.container}>
          <ProfileInfo/>
        </div>
      </div>
    </Layout>
  );
}

Profile.propTypes = {
  title: PropTypes.string.isRequired,
};

export default withStyles(s)(Profile);
