//profile
import React from 'react';
import Profile from './Profile';

const title = 'Profile';

export default {

  path: '/profile',

  async action() {
    return {
      title: 'Home',
      component: <Profile title={title}/>,
    };
  },

};
