import React, { PropTypes } from 'react';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import Layout from '../../components/Layout';
import RegisterCoNotes from '../../components/RegisterCoNotes/RegisterCoNotes';
import s from './Register.css';
import Link from '../../components/Link';

// Register route that uses the register component

function Register({ title }) {
  return (
    <Layout>
      <div className={s.root}>
        <div className={s.container}>
          <h1>{title}</h1>
          <RegisterCoNotes/>
          Already a user? <Link to="/login">Log In! </Link>
        </div>
      </div>
    </Layout>
  );
}

Register.propTypes = { title: PropTypes.string.isRequired };

export default withStyles(s)(Register);
