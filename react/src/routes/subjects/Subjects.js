import React, { PropTypes } from 'react';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import Layout from '../../components/Layout';
import SubjectChart from '../../components/SubjectChart/SubjectChart';
import SubjectList from '../../components/SubjectList/SubjectList';
import s from './Subjects.css';

// List of available subjects and their notes

function Subjects({ title }) {
  return (
    <Layout>
      <div className={s.root}>
        <div className={s.container}>

          <SubjectList/>

          <span id="name"> Chart depicting the distribution of texts per subject: </span>
          <div className={s.chart}>
          <SubjectChart/>

        </div>
        </div>
      </div>
    </Layout>
  );
}

Subjects.propTypes = {
  title: PropTypes.string.isRequired,
};

export default withStyles(s)(Subjects);