import React from 'react';
import Subjects from './Subjects';

const title = 'Subjects';

export default {

  path: '/subjects',

  async action() {
    return {
      title,
      component: <Subjects title={title} />,
    };
  },

};
