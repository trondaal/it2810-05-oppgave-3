import React, { PropTypes } from 'react';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import Layout from '../../components/Layout';
import TextList from '../../components/TextList';
import s from './Texts.css';

// Route for displaying made texts and notes

function Texts({ title }) {
  return (
    <Layout>
      <div className={s.root}>
        <div className={s.container}>
          <TextList/>
        </div>
      </div>
    </Layout>
  );
}

Texts.propTypes = {
  title: PropTypes.string.isRequired,
};

export default withStyles(s)(Texts);
