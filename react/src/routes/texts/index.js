import React from 'react';
import Texts from './Texts';

const title = 'Texts';

export default {

  path: '/texts',

  async action() {
    return {
      title,
      component: <Texts title={title} />
    };
  },

};
