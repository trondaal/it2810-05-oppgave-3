import 'babel-polyfill';
import path from 'path';
import React from 'react';
import assert from 'assert';
import routes from './routes';
import assets from './assets'; // eslint-disable-line import/no-unresolved
import express from 'express';
import {port} from './config';
import jwt from 'jsonwebtoken';
import mongoose from 'mongoose';
import SocketIo from 'socket.io';
import MongoClient from 'mongodb';
import App from './components/App';
import bodyParser from 'body-parser';
import expressJwt from 'express-jwt';
import Html from './components/Html';
import PrettyError from 'pretty-error';
import cookieParser from 'cookie-parser'
import ReactDOM from 'react-dom/server';
import expressGraphQL from 'express-graphql';
import UniversalRouter from 'universal-router';
import errorPageStyle from './routes/error/ErrorPage.css';
import {ErrorPageWithoutStyle} from './routes/error/ErrorPage';
import {login, register, getInfo, changePassword, addTextToUser, getTexts} from './controllers/user.controller';
import {saveCourse, addTextToCourse, getAll, getChartData, createStartupCourse} from './controllers/course.controller';
import {userTextsBySubject, getTextOnId, createStartupText, saveExistingText, saveText} from './controllers/text.controller';


var http = require("http");
var session = require('express-session');
const MongoStore = require('connect-mongo')(session);  //Used for session handling
const app = express();

var Text = require('./models/text.model');
var User = require('./models/user.model');

//
// Tell any CSS tooling (such as Material UI) to use all vendor prefixes if the
// user agent is not known.
// -----------------------------------------------------------------------------
global.navigator = global.navigator || {};
global.navigator.userAgent = global.navigator.userAgent || 'all';

//
// Register Node.js middleware
// -----------------------------------------------------------------------------

app.use(express.static(path.join(__dirname, 'public')));
app.use(cookieParser());
app.use(session({
  resave: true,
  saveUninitialized: true,
  secret: 'SOMERANDOMSECRETHERE',
  store: new MongoStore({url: 'mongodb://localhost:27017/coNotes'}),
  cookie: {maxAge: null}
})); //Session handling via MongoDB
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());


//

//
// Register API middleware
// -----------------------------------------------------------------------------
app.use('/graphql', expressGraphQL(req => ({
  schema,
  graphiql: true,
  rootValue: {request: req},
  pretty: process.env.NODE_ENV !== 'production',
})));

// POST and GET listens
//------------------------------------------------------------------------------

// Listens for signup attempts
app.post('/signup', function (req, res) {
  register(req.body.email, req.body.password, function (err, user) {
    if (user) {
      res.send('success');
    }
    else {
      res.send('alreadyExists');
    }
  });
});

// Listens for change password attempts
app.post('/changePassword', function (req, res) {
  changePassword(req.body.email, req.body.password, function (err, feedback) {
    res.send(feedback);
  });
});

// Listens for getting a users texts
app.post('/getUserTexts', function (req, res) {
  getTexts(req.session.email, function (err, texts) {
    res.send(texts);
  });
});

// Listens for getting texts belonging to a subject/course
app.post('/getSubjectTexts', function (req, res) {
  getTexts(req.session.email, function (err, texts) {
    userTextsBySubject(texts, req.body.subject, function (err, courses) {
      res.send(courses);
    });
  });
});


// Listens for getting all courses and pusnhing them into custom array
app.get('/getAllCourses', function (req, res) {

  getAll(function (err, courses) {
    var results = []
    if (courses) {
      courses.forEach(function (entry) {
        var temp = [];
        temp.push(entry.name);
        temp.push(entry.faculty);
        results.push(temp);
      })
    }
    res.send(results);
  });
});


// Listens for requests to get an array to create a subject chart
app.get('/getChartData', function (req, res) {
  getChartData(function (err, data) {
    if (data) {
      res.send(data);
    }
  });
});

// Listens for login attempts
app.post('/authorize', function (req, res) {
  login(req.body.email, req.body.password, function (err, user) {
    if (user) {
      //Creates session if successful login
      req.session.regenerate(function () {
        req.session.user = user;
        req.session.email = req.body.email;
        res.send('loginSuccess');
      });
    } else {
      res.send('loginFail');
    }
  });
});


//returns the email of the user's session
app.get('/userEmail', function (req, res) {
  res.send(req.session.email);
});

//returns the user-object of the current user, from the DB
app.get('/userInfo', function (req, res) {
  getInfo(req.session.email, function (err, user) {
    res.send(user);
  });
});



// Listens for logout attemps
app.post('/logout', function (req, res) {
  if (req.session.user) {
    console.log("User logged out")
    req.session.destroy();
    res.send('/');
  }
  else {
    console.log("No user logged in")
  }
});


// Listens for attempts to create text
app.post('/createText', function (req, res) {
  //Save texts
  saveText({name: req.body.name, course: req.body.course, creator: req.session.email}, function (err, text) {
    if (err) {
      console.log(err);
    }
    // If text created and saved successfully
    else {
      // Adds text to course's array of texts
      addTextToCourse(req.body.course, req.body.faculty, req.body.name, function (err, course) {
        if (err) {
          console.log(err);
        }
      });
      // Adds text to user's array of texts
      addTextToUser(req.session.email, req.body.name, function (err, user) {
        if (err) {
          console.log(err);
        }
      });
    }
  });
});

app.post('/saveExistingText', function(req, res){
  saveExistingText(req.body.name, req.body.content, function(err, text){
    res.send('Success');
  });
});






//
// Register server-side rendering middleware
// -----------------------------------------------------------------------------
app.get('*', async(req, res, next) => {
  try {
    const css = new Set();

    // Global (context) variables that can be easily accessed from any React component
    // https://facebook.github.io/react/docs/context.html
    const context = {
      // Enables critical path CSS rendering
      // https://github.com/kriasoft/isomorphic-style-loader
      insertCss: (...styles) => {
        // eslint-disable-next-line no-underscore-dangle
        styles.forEach(style => css.add(style._getCss()));
      },
    };

    const route = await UniversalRouter.resolve(routes, {
      path: req.path,
      query: req.query,
    });




    if (route.redirect) {
      res.redirect(route.status || 302, route.redirect);
      return;
    }

    const data = {...route};
    data.children = ReactDOM.renderToString(<App context={context}>{route.component}</App>);
    data.style = [...css].join('');
    data.script = assets.main.js;
    const html = ReactDOM.renderToStaticMarkup(<Html {...data} />);

    res.status(route.status || 200);
    res.send(`<!doctype html>${html}`);
  } catch (err) {
    next(err);
  }
});

//
// Error handling
// -----------------------------------------------------------------------------
const pe = new PrettyError();
pe.skipNodeFiles();
pe.skipPackage('express');

app.use((err, req, res, next) => { // eslint-disable-line no-unused-vars
  console.log(pe.render(err)); // eslint-disable-line no-console
  const html = ReactDOM.renderToStaticMarkup(
    <Html
      title="Internal Server Error"
      description={err.message}
      style={errorPageStyle._getCss()} // eslint-disable-line no-underscore-dangle
    >
    {ReactDOM.renderToString(<ErrorPageWithoutStyle error={err}/>)}
    </Html>
  );
  res.status(err.status || 500);
  res.send(`<!doctype html>${html}`);
});

//
// Launch the server
// -----------------------------------------------------------------------------
// Connect to database

mongoose.Promise = global.Promise;
mongoose.connect('mongodb://localhost:27017/coNotes');

/*
 var databaseUrl = 'mongodb://localhost:27017/coNotes';

 try {
 MongoClient.connect(databaseUrl, function(err, db) {
 assert.equal(null, err);
 console.log("Connected successfully to server");

 //db.close();
 });
 }
 catch(err) {
 console.log(err.message);
 }
 */

/* eslint-disable no-console */
const server = app.listen(port, () => {
  console.log(`The server is running at http://localhost:${port}/`);
});
/* eslint-enable no-console */

const io = SocketIo.listen(server);

app.post('/getTextName', function(req, res){
  getTextOnId(req.body.name, function(err, text){
    console.log("TEXT: ", text);
    res.send(text);
  });


  var nsp = io.of('/'+req.body.name);

  //Receives all socket messages from client.
  nsp.on('connection', function(socket){
    console.log('a user connected');
    socket.on('disconnect', function(){
      console.log('user disconnected');
    });
    //Receives message with new keyinput from editor and broadcasts it to other clients.
    socket.on('textInput', function(msg){


      console.log('Message '+ msg)
      //socket.emit('update', msg);
      socket.broadcast.emit('push', msg);
    });
    socket.on('save', function(msg){
      saveText(msg);
      addTextToCourse(msg.course, msg.name);
    });

  });
});

//Startup script, dummy data////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Helping function for creating texts:
function createText(creator, textName, course, faculty) {
  saveText({name: textName, course: course, creator: creator}, function (err, text) {
    if (text) {
      addTextToCourse(course, faculty, textName, function (err, course) {
      });
      addTextToUser(creator, textName, function (err, user) {
      });
    }
  });
}

User.count({}, function (err, count) {
  if (count == 0) {

  }
})
//Checks if texts are empty, works because texts will only be empty if startup script has not been ran
Text.count({}, function (err, count) {
  if (count == 0) {
    //Adds lots of users, A is admin user
    register("A", "A", function (err, user) {
    });
    register("sondre@conotes.com", "sondre", function (err, user) {
    });
    register("ole@conotes.com", "ole", function (err, user) {
    });
    register("egil@conotes.com", "egil", function (err, user) {
    });
    register("wiker@conotes.com", "wiker", function (err, user) {
    });
    register("frode@conotes.com", "frode", function (err, user) {
    });
    register("petter@conotes.com", "petter", function (err, user) {
    });
    //Uses timeout to avoid issues as the functions are running asynchronous
    setTimeout(function () {
      //Adds lots of text, and adds them to a creator which is a user, also creates courses with faculties
      createText("A", "Geo1", "Geology", "IME");
      createText("A", "Geo2", "Geology", "IME");
      createText("sondre@conotes.com", "Geo3", "Geology", "IME");
      createText("sondre@conotes.com", "Psy1", "Psychology", "HF");
      createText("sondre@conotes.com", "Med1", "Medicine for Non-Medical Students,", "DMF");
      createText("ole@conotes.com", "Med2", "Medicine for Non-Medical Students,", "DMF");
      createText("ole@conotes.com", "Oop1", "OOP", "IDI");
      createText("ole@conotes.com", "Oop2", "OOP", "IDI");
      createText("egil@conotes.com", "Cal1", "Calculus", "IME");
      createText("egil@conotes.com", "Che1", "Chemistry", "IME");
      createText("egil@conotes.com", "Con1", "Construction", "IVT");
      createText("wiker@conotes.com", "Psy2", "Psychology", "HF");
      createText("wiker@conotes.com", "Med3", "Medicine for Non-Medical Students,", "DMF");
      createText("wiker@conotes.com", "Oop3", "OOP", "IDI");
      createText("frode@conotes.com", "Cal2", "Calculus", "IME");
      createText("frode@conotes.com", "Oop4", "OOP", "IDI");
      createText("frode@conotes.com", "Che2", "Chemistry", "IME");
      createText("petter@conotes.com", "Oop5", "OOP", "IDI");
      createText("petter@conotes.com", "Geo4", "Geology", "IME");
      createText("petter@conotes.com", "Con2", "Construction", "IVT");
    }, 8000);
  }
});
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
